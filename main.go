package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

const (
	standBy = "StandBy"
	running = "Running"
	stopped = "Stopped"
	paused  = "Paused"
)

// Task is task
type Task interface {
	doJob()
	setStatus(status string)
	getStatus() string
	getName() string
}

// TaskInfo task information
type TaskInfo struct {
	Name   string
	Status string
}

func (t TaskInfo) doJob() {
	fmt.Printf("%s is executed at %v\n", t.Name, time.Now())
	fmt.Println()
}

func (t *TaskInfo) setStatus(status string) {
	t.Status = status
}

func (t *TaskInfo) getStatus() string {
	return t.Status
}

func (t *TaskInfo) getName() string {
	return t.Name
}

func doSomething(t Task) {
	t.doJob()
}

func main() {
	storage := []Task{}
	var wg sync.WaitGroup

	wg.Add(2)

	uptimeTicker := time.NewTicker(15 * time.Second)

	go func() {
		defer wg.Done()
		for {
			fmt.Println("Hello, What do you want to do? Please choose a NUMBER:")
			fmt.Println("0. View all tasks")
			fmt.Println("1. Add a new task")
			fmt.Println("2. Start an added task")
			fmt.Println("3. Stop the running task")
			fmt.Println("4. Pause the running task")
			fmt.Println("5. Remove the selected task")
			fmt.Println("6. Import tasks from file")
			fmt.Println("7. Export tasks to file")

			var input string
			fmt.Scanln(&input)
			fmt.Println()

			switch input {
			case "0":
				for i, t := range storage {
					fmt.Printf("%v. %s\n", i+1, t)
				}
				fmt.Println()
			case "1":
				data := TaskInfo{
					Name:   "Task " + strconv.Itoa(len(storage)+1),
					Status: standBy,
				}

				var t Task = &data
				storage = append(storage, t)

			case "2":
				fmt.Println("Which task do you want to start? Please choose a NUMBER:")
				for i, t := range storage {
					fmt.Printf("%v. %s\n", i+1, t)
				}
				var input2 string
				fmt.Scanln(&input2)
				fmt.Println()
				lenghtTasks := len(storage)

				i2, _ := strconv.Atoi(input2)
				if i2 > lenghtTasks {
					fmt.Printf("Please input a number less than %v\n", lenghtTasks)
					fmt.Println()
				} else if i2 >= 0 && i2 <= lenghtTasks {
					switch storage[i2-1].getStatus() {
					case running:
						fmt.Printf("Task %v is already running\n", input2)
						fmt.Println()
					case standBy:
						storage[i2-1].setStatus(running)
						doSomething(storage[i2-1])
					case paused:
						storage[i2-1].setStatus(running)
						doSomething(storage[i2-1])
					default:
						fmt.Println("Task is already stopped")
					}
				} else {
					fmt.Println("Please add a correct number")
					fmt.Println()
				}

			case "3":
				fmt.Println("Which task do you want to stop? Please choose a NUMBER:")
				for i, t := range storage {
					fmt.Printf("%v. %s\n", i+1, t)
				}
				var input2 string
				fmt.Scanln(&input2)
				fmt.Println()
				lenghtTasks := len(storage)

				i2, _ := strconv.Atoi(input2)
				if i2 > lenghtTasks {
					fmt.Printf("Please input a number less than %v\n", lenghtTasks)
					fmt.Println()
				} else if i2 >= 0 && i2 <= lenghtTasks {
					switch storage[i2-1].getStatus() {
					case running:
						storage[i2-1].setStatus(stopped)
					case stopped:
						fmt.Printf("Task %v is already stopped\n", input2)
						fmt.Println()
					default:
						fmt.Println("Task must be running to be stopped")
					}
				} else {
					fmt.Println("Please add a correct number")
					fmt.Println()
				}

			case "4":
				fmt.Println("Which task do you want to pause? Please choose a NUMBER:")
				for i, t := range storage {
					fmt.Printf("%v. %s\n", i+1, t)
				}
				var input2 string
				fmt.Scanln(&input2)
				fmt.Println()
				lenghtTasks := len(storage)

				i2, _ := strconv.Atoi(input2)
				if i2 > lenghtTasks {
					fmt.Printf("Please input a number less than %v\n", lenghtTasks)
					fmt.Println()
				} else if i2 >= 0 && i2 <= lenghtTasks {
					switch storage[i2-1].getStatus() {
					case running:
						storage[i2-1].setStatus(paused)
					case paused:
						fmt.Printf("Task %v is already paused\n", input2)
						fmt.Println()
					default:
						fmt.Println("Task must be running to be stopped")
						fmt.Println()
					}
				} else {
					fmt.Println("Please add a correct number")
					fmt.Println()
				}

			case "5":
				fmt.Println("Which task do you want to remove? Please choose a NUMBER:")
				for i, t := range storage {
					fmt.Printf("%v. %s\n", i+1, t)
				}
				var input2 string
				fmt.Scanln(&input2)
				fmt.Println()
				lenghtTasks := len(storage)

				i2, _ := strconv.Atoi(input2)
				if i2 > lenghtTasks {
					fmt.Printf("Please input a number less than %v\n", lenghtTasks)
					fmt.Println()
				} else if i2 >= 0 && i2 <= lenghtTasks {
					storage = append(storage[:i2-1], storage[i2:]...)
				} else {
					fmt.Println("Please add a correct number")
					fmt.Println()
				}

			case "6":
				fmt.Println("Importing...")
				fmt.Println("...")

				f, err := os.Open("demo.csv")
				if err != nil {
					log.Fatalln("failed to open file", err)
				}
				defer f.Close()

				r := csv.NewReader(f)

				// skip first line
				if _, err := r.Read(); err != nil {
					log.Fatalln("failed to read file", err)
				}

				records, err := r.ReadAll()
				if err != nil {
					log.Fatalln("failed to read all file", err)
				}

				for _, record := range records {
					task := TaskInfo{
						Name:   record[0],
						Status: record[1],
					}

					var t Task = &task
					storage = append(storage, t)
				}

				fmt.Println("...")
				fmt.Println("...Done")

			case "7":
				records := [][]string{{"Name", "Status"}}

				for _, t := range storage {
					a := []string{t.getName(), t.getStatus()}

					records = append(records, a)
				}

				f, err := os.Create("tasks.csv")
				defer f.Close()

				if err != nil {
					log.Fatalln("failed to open file", err)
				}

				w := csv.NewWriter(f)
				defer w.Flush()

				err = w.WriteAll(records)
				if err != nil {
					log.Fatalln("error writing record to file", err)
				}

				fmt.Println("...")
				fmt.Println("...Done")
				fmt.Println()
			}
		}
	}()

	go func() {
		defer wg.Done()
		for {
			select {
			case <-uptimeTicker.C:
				for _, t := range storage {
					if t.getStatus() == running {
						doSomething(t)
					}
				}
			}
		}
	}()

	wg.Wait()
}
